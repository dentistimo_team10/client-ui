/**
 * This file initalises VueMqtt, Vuex and the App.vue
 * Author(s): Hannah, Lucas
 */

import Vue from 'vue'
import BootstrapVue from 'bootstrap-vue'
import VueMqtt from 'vue-mqtt'
import UUID from 'vue-uuid'

import 'bootstrap/dist/css/bootstrap.css'
import 'bootstrap-vue/dist/bootstrap-vue.css'
import App from './App.vue'
import router from './utilities/router'
import { store } from './utilities/store'

Vue.use(BootstrapVue)
Vue.use(UUID)

Vue.config.productionTip = false
const { v4: uuidv4 } = require('uuid')
const options = {
  host: 'broker.emqx.io',
  port: 8083,
  protocol: 'ws',
  endpoint: '/mqtt',
  clean: true,
  connectTimeout: 4000,
  reconnectPeriod: 4000,
  clientId: uuidv4(),
  username: 'team-10',
  password: 'team-10'
}

const connectUrl = `${options.protocol}://${options.host}:${options.port}${options.endpoint}`
Vue.use(VueMqtt, connectUrl, options)

new Vue({
  router,
  store: store,
  render: function (h) { return h(App) }
}).$mount('#app')
